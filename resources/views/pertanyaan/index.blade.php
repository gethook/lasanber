@extends('adminlte.master')
@section('title')
	Daftar Pertanyaan
@endsection

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card">
				{{-- <div class="card-header">
					<h3 class="card-title">Pertanyaan</h3>
				</div> --}}
				<div class="card-body">
					@if (session('sukses'))
						<div class="alert alert-success">
							{{session('sukses')}}
						</div>
					@endif
					<div class="row">
						<div class="col-12 mb-3">
							<a href="/pertanyaan/create" class="btn btn-primary">
								<i class="fas fa-plus fa-fw"></i>
								Buat Pertanyaan
							</a>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover" id="example1">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Title</th>
									<th class="text-center">Body</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($daftar_pertanyaan as $key_p => $pertanyaan)
									<tr>
										<td>{{$key_p + 1}}</td>
										<td>{{$pertanyaan->judul}}</td>
										<td>{{\Illuminate\Support\Str::limit(strip_tags($pertanyaan->isi), 20)}}</td>
										<td>
											<div class="d-flex justify-content-center">
												<a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-sm ml-1 btn-info">
													show
												</a>
												<a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-sm ml-1 btn-warning">
													edit
												</a>
												<form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-sm ml-1 btn-danger">hapus</button>
												</form>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endpush