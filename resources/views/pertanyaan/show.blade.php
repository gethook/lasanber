@extends('adminlte.master')

@section('title')
	View pertanyaan
@endsection

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">{{$pertanyaan->judul ?? 'Tidak ada data'}}</h3>
				</div>
				<div class="card-body">
					<div class="card-text">
						{!!$pertanyaan->isi ?? 'tidak ada data'!!}
					</div>
				</div>
				<div class="card-footer">
					<a href="/pertanyaan" class="btn btn-warning">
						<i class="fas fa-reply fa-fw"></i>
						Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection