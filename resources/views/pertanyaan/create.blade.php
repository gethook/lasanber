@extends('adminlte.master')

@section('title')
	Buat Pertanyaan Baru
@endsection

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Pertanyaan Baru</h3>
				</div>
				<div class="card-body">
					<form role="form" method="post" action="/pertanyaan">
						@csrf
					    <div class="form-group">
					      <label for="judul">Judul</label>
					      <input type="text" class="form-control <?php echo $errors->has('judul') ? 'is-invalid' : ''; ?>" id="judul" placeholder="Judul Pertanyaan" name="judul" value="{{old('judul', '')}}">
					      @error('judul')
					      	<div class="invalid-feedback">{{$message}}</div>
					      @enderror
					    </div>
					    <div class="form-group">
					      <label for="isi">Isi pertanyaan</label>
					      <textarea class="textarea  <?php echo $errors->has('isi') ? 'is-invalid' : ''; ?>" placeholder="Tulis pertanyaan disini" id="isi" name="isi" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('isi', '')}}</textarea>
                          @error('isi')
                          	<div class="invalid-feedback">{{$message}}</div>
                          @enderror
					    </div>
					    <button type="submit" class="btn btn-primary">Submit</button>
					</form>

				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte')}}/plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endpush

@push('styles')
	<link rel="stylesheet" href="{{asset('adminlte')}}/plugins/summernote/summernote-bs4.css">
@endpush