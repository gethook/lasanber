<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SanberBook - Sign Up</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<p><label for="fname">First name:</label></p>
		<p><input type="text" id="fname" name="fname"></p>
		<p><label for="lname">Last name:</label></p>
		<p><input type="text" id="lname" name="lname"></p>
		<p>Gender:</p>
		<input type="radio" id="male" name="gender" value="male"> 
		<label for="male">Male</label> <br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label> <br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label> <br>
		<p>Nationality</p>
		<p>
			<select name="nationality" id="nationality">
				<option value="id">Indonesia</option>
				<option value="en">Inggris</option>
				<option value="ar">Arab</option>
			</select>
		</p>
		<p>Language Spoken</p>
		<input type="checkbox" name="lan" id="id" value="indo">
		<label for="id">Bahasa Indonesia</label><br>
		<input type="checkbox" name="lan" id="en" value="eng">
		<label for="en">English</label><br>
		<input type="checkbox" name="lan" id="ot" value="other">
		<label for="ot">Other</label><br>
		<p>Bio</p>
		<textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>