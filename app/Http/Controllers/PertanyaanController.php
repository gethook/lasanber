<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
    	$daftar_pertanyaan = DB::table('pertanyaan')->get(); // select * from pertanyaan
    	return view('pertanyaan.index', compact('daftar_pertanyaan'));
    }

    public function create()
    {
    	return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'judul' => 'required|unique:pertanyaan|string|min:10',
    		'isi' => 'required|string|min:50'
    	]);
    	$pertanyaan = array(
    		'judul' => $request['judul'],
    		'isi'  => $request['isi'],
    		'profil_id' => 1,
    	);
    	$query = DB::table('pertanyaan')->insert($pertanyaan);
    	return redirect('/pertanyaan')->with('sukses', 'Pertanyaan berhasil dibuat');
    }

    public function show($id)
    {
    	$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
    	$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
    	$request->validate([
    		'judul' => 'required|string|min:10',
    		'isi' => 'required|string|min:50'
    	]);
    	$query = DB::table('pertanyaan')
    		->where('id', $id)
    		->update([
    			'judul' => $request['judul'],
    			'isi'  => $request['isi'],
    		]);
    	return redirect('/pertanyaan')->with('sukses', 'Berhasil update pertanyaan.');
    }

    public function destroy($id)
    {
    	$query = DB::table('pertanyaan')->where('id', $id)->delete();
    	return redirect('/pertanyaan')->with('sukses', 'Pertanyaan ' . $id . ' berhasil dihapus.');
    }
}
